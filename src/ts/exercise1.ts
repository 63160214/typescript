interface Menu {
    name: string 
    subMenu: SubMenu[]
  }
   
  interface SubMenu {
    name: string
  }
   
  const menus: Menu[] = [
    {
      name: 'Home',
      subMenu: [],
    },
    {
      name: 'About',
      subMenu: [
        {
          name: 'Company',
        },
        {
          name: 'Team',
        },
      ],
    },
    {
      name: 'Products',
      subMenu: [
        {
          name: 'Electronics',
        },
        {
          name: 'Clothing',
        },
        {
          name: 'Accessories',
        },
      ],
    },
    {
      name: 'Services',
      subMenu: [],
    },
    {
      name: 'Contact',
      subMenu: [
        {
          name: 'Phone',
        },
      ],
    },
    {
      name: 'Blog',
      subMenu: [],
    },
    {
      name: 'Gallery',
      subMenu: [
        {
          name: 'Photos',
        },
        {
          name: 'Videos',
        },
        {
          name: 'Events',
        },
      ],
    },
    {
      name: 'FAQ',
      subMenu: [],
    },
    {
      name: 'Downloads',
      subMenu: [
        {
          name: 'Documents',
        },
        {
          name: 'Software',
        },
      ],
    },
    {
      name: 'Support',
      subMenu: [
        {
          name: 'Help Center',
        },
        {
          name: 'Contact Us',
        },
        {
          name: 'Knowledge Base',
        },
      ],
    },
  ];
const div = document.getElementById("app") as HTMLDivElement
const t = document.createElement("ul") as HTMLUListElement

for (const menu of menus) {
  const n: HTMLLIElement = document.createElement("li");
  const u: Text = document.createTextNode(menu.name);
  n.appendChild(u);

  if (menu.subMenu.length > 0) {
    const a: HTMLUListElement = document.createElement("ul");
    for (const o of menu.subMenu) {
      const m: HTMLLIElement = document.createElement("li");
      const s: Text = document.createTextNode(o.name);
      m.appendChild(s);
      a.appendChild(m);
    }
    n.appendChild(a);
  }

  t.appendChild(n);
}

  div.appendChild(t);
