interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
  }
   
  const questions: Question[] = [
    {
      question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
      choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to add one or more elements to the end of an array?',
      choices: ['push()', 'join()', 'slice()', 'concat()'],
      correctAnswer: 0,
    },
    {
      question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
      choices: ['"327"', '"12"', '"57"', '"NaN"'],
      correctAnswer: 2,
    },
    {
      question: 'What is the purpose of the "use strict" directive in JavaScript?',
      choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
      correctAnswer: 2,
    },
    {
      question: 'What is the scope of a variable declared with the "let" keyword?',
      choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
      correctAnswer: 2,
    },
    {
      question: 'Which higher-order function is used to transform elements of an array into a single value?',
      choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
      correctAnswer: 2,
    },
    {
      question: 'What does the "=== " operator in JavaScript check for?',
      choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
      correctAnswer: 1,
    },
    {
      question: 'What is the purpose of the "this" keyword in JavaScript?',
      choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
      correctAnswer: 3,
    },
    {
      question: 'What does the "NaN" value represent in JavaScript?',
      choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to remove the last element from an array?',
      choices: ['pop()', 'shift()', 'slice()', 'splice()'],
      correctAnswer: 0,
    },
  ];

const m = document.getElementById("app") as HTMLElement
const a = document.createElement("div") as HTMLDivElement
a.innerText = "Current Score: 0/10";
if (m) {
  m.appendChild(a);
}

let f: number = 0;

for (const { question: v, choices: l, correctAnswer: b } of questions) {
  const e: HTMLDivElement = document.createElement("div");
  const d: HTMLParagraphElement = document.createElement("p");
  const h: HTMLDivElement = document.createElement("div");
  const n: HTMLButtonElement = document.createElement("button");
  const r: HTMLParagraphElement = document.createElement("p");

  d.innerText = v;
  const i: HTMLInputElement[] = [];
  let c: number | null = null;

  for (let t = 0; t < l.length; t++) {
    const u: number = t;
    const s: HTMLDivElement = document.createElement("div");
    const o: HTMLInputElement = document.createElement("input");
    o.type = "radio";
    const p: HTMLSpanElement = document.createElement("span");
    p.innerText = l[u];

    o.addEventListener("change", () => {
      for (const w of i) w.checked = false;
      o.checked = true;
      c = u;
    });

    i.push(o);
    s.appendChild(o);
    s.appendChild(p);
    h.appendChild(s);
  }

  n.innerText = "Submit";
  n.addEventListener("click", () => {
    if (c == null) {
      alert("please choose an answer first!");
      return;
    }

    for (const t of i) t.disabled = true;
    n.disabled = true;

    if (c === b) {
      r.innerText = "Correct!";
      f++;
      a.innerText = `Current Score: ${f}/10`;
    } else {
      r.innerText = "Incorrect!";
    }
  });

  e.appendChild(d);
  e.appendChild(h);
  e.appendChild(n);
  e.appendChild(r);

  if (m) {
    m.appendChild(e);
  }
}